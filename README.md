Resource Action authorization and mandate eXpressions
=====================================================

**RAX**


NFA building and test based on 
["Algorithms, Fourth Edition", "5.4 Regular Expressions"; Robert Sedgewick, Kevin Wayne (PDF)](https://algs4.cs.princeton.edu/lectures/54RegularExpressions.pdf)
(also at [github](https://github.com/prakashn27/Algorithms/blob/master/Princeton/54RegularExpressions.pdf)).

converting the NFA to a DFA, and minimizing it, is based on
[Constructing a minimum-state DFA from a Regular Expression](https://web.archive.org/web/20120702185839/http://www.cs.oberlin.edu/~jdonalds/331/lecture05.html)
