const specialTransitionTypes = ['ε', 'any', 'anyOrSlash']

const KLEENE_STAR = 'KLEENE STAR'

function State (id) {
  this.id = id
}
Object.assign(
  State.prototype,
  {
    constructor: State,
    invariants: [
      function () { return typeof this.id === 'number' },
      function () { return !this.transition || this.transition instanceof State },
      function () { return Array.isArray(this.ε) },
      function () { return this.ε.every(function (t) { return t instanceof State }) }
    ],
    compare: function (other) {
      return this.id < other.id ? -1 : (this.id === other.id ? 0 : +1)
    },
    transition: undefined,
    ε: [],
    εClosure: function (reachable) {
      if (reachable.indexOf(this) >= 0) {
        return reachable
      }
      reachable.push(this)
      return reachable
    },
    visit: function (visitor, acc, visited) {
      var self = this
      if (visited.indexOf(this) >= 0) {
        return acc
      }
      visited.push(this)
      var visitedAcc = visitor(acc, self)
      if (this.transition) {
        visitedAcc = this.transition.visit(visitor, acc, visited)
      }
      return self.ε.reduce(function (acc, ε) { return ε.visit(visitor, acc, visited) }, visitedAcc)
    },
    doesTransition: function (ignore) {
      return false
    }
  }
)
State.isNormalAcceptableTransition = function (char) {
  return !!char && typeof char === 'string' && true
}
State.isAcceptableTransition = function (char) {
  return specialTransitionTypes.indexOf(char) >= 0 || State.isAcceptableTransition(char)
}

function Accepting (id) {
  State.call(this, id)
}
Accepting.prototype = Object.assign(
  new State(),
  {
    constructor: Accepting,
    invariants: State.prototype.invariants.concat([
      function () { return !this.transition },
      function () { return this.ε.length === 0 }
    ])
  }
)

const transitionChar = /:|\/|[a-zA-Z]|\d/
const starTransitionChar = /[a-zA-Z]|\d/ // TODO in glob, does contain :; protect against this
const transitionToken = /:|\/|[a-zA-Z]|\d|{|,|}|\*\*|\?/

/**
 * {@param token} is the token that needs to be matched to proceed over {@code transition} from this state.
 */
function NotAccepting (id, token) {
  console.assert(!token || transitionToken.test(token) || token === KLEENE_STAR)
  State.call(this, id)
  this.token = token
  this.ε = []
}
// noinspection SpellCheckingInspection
NotAccepting.prototype = Object.assign(
  new State(),
  {
    constructor: NotAccepting,
    invariants: [
      function () { return typeof this.token === 'string' },
      function () {
        return !this.token ||
               transitionToken.test(this.token) || // NOTE: no single * as token - this is ?
               this.token === KLEENE_STAR
      }
    ],
    doesTransition: function (char) {
      if (!this.transition) {
        return false
      }
      if (this.token === '**') {
        return transitionChar.test(char)
      }
      if (this.token === '?') {
        return starTransitionChar.test(char)
      }
      if (transitionChar.test(this.token)) {
        return char === this.token
      }
      return State.prototype.doesTransition.call(this, char)
    },
    addε: function (state) {
      this.ε.push(state)
    },
    εClosure: function (reachable) {
      if (reachable.indexOf(this) >= 0) {
        return reachable
      }
      reachable.push(this)
      return this.ε.reduce(function (acc, t) { return t.εClosure(acc) }, reachable)
    }
  }
)

function StartState (id, token) {
  NotAccepting.call(this, id, token)
}
StartState.prototype = Object.assign(
  new NotAccepting(),
  {
    constructor: StartState,
    visit: function (visitor, acc) {
      return NotAccepting.prototype.visit.call(this, visitor, acc, [])
    },
    test: function (str) {
      var reachable = this.εClosure([])
      var strIndex = 0
      do {
        console.log('reachable: [' + reachable.map(function (r) { return r.id + ' "' + r.token + '"' }).join(', ') + ']')
        console.log('checking "' + str.charAt(strIndex) + '"')
        reachable = reachable
          .filter(function (state) { return state.doesTransition(str.charAt(strIndex)) })
          .reduce(function (acc, state) { return state.transition.εClosure(acc) }, [])
        strIndex++
      } while (strIndex < str.length && reachable.length > 0)
      return strIndex >= str.length && reachable.some(function (r) { return r instanceof Accepting })
    }
  }
)

module.exports = {
  KLEENE_STAR: KLEENE_STAR,
  transitionChar: transitionChar,
  starTransitionChar: starTransitionChar,
  transitionToken: transitionToken,
  State: State,
  Accepting: Accepting,
  NotAccepting: NotAccepting,
  Start: StartState
}
