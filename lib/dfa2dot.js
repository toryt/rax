function transition2Dot (from, to, transition) {
  return 's' + from.id + ' -> s' + to.id + ' [label="' + transition + '"]'
}

function dfa2dot (dfa) {
  const transitions = dfa.visit(
    function (visitAcc, state) {
      var stateRepresentation = 's' + state.id + ' [label="' + state.id + '" xlabel="' + state.key + '"'
      if (state.start) {
        stateRepresentation += ' fillcolor="lightblue" style="filled"'
      } else if (state.accepting) {
        stateRepresentation += ' fillcolor="green" style="filled"'
      }
      stateRepresentation += ']'
      visitAcc.push(stateRepresentation)
      return Object.keys(state.transitions).reduce(
        function (acc, t) {
          acc.push(transition2Dot(state, state.transitions[t], t))
          return acc
        },
        visitAcc
      )
    },
    []
  )
  return transitions.reduce(
    function (acc, t) { return acc + '  ' + t + '\n' },
    'digraph "DFA" {\n  rankdir=LR\n\n'
  ) + '}'
}

module.exports = dfa2dot
