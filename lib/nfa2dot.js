const states = require('./nfa')

function transition2Dot (from, to, transition) {
  const color = transition === 'ε' ? 'red' : 'blue'
  var result = 's' + from.id + ' -> s' + to.id + ' [color=' + color
  if (transition !== 'ε') {
    result += ' label="' + transition + '"'
  }
  result += ']'
  return result
}

function nfa2dot (nfa) {
  const transitions = nfa.visit(
    function (visitAcc, state) {
      var stateRepresentation = 's' + state.id + ' [label="' + state.id + ' ' + state.token + '"'
      if (state instanceof states.Start) {
        stateRepresentation += ' fillcolor="lightblue" style="filled"'
      } else if (state instanceof states.Accepting) {
        stateRepresentation += ' fillcolor="green" style="filled"'
      }
      stateRepresentation += ']'
      visitAcc.push(stateRepresentation)
      if (state.transition) {
        visitAcc.push(transition2Dot(state, state.transition, state.token))
      }
      return state.ε.reduce(
        function (acc, to) {
          acc.push(transition2Dot(state, to, 'ε'))
          return acc
        },
        visitAcc
      )
    },
    []
  )
  return transitions.reduce(
    function (acc, t) { return acc + '  ' + t + '\n' },
    'digraph "NFA" {\n  rankdir=LR\n\n'
  ) + '}'
}

module.exports = nfa2dot
