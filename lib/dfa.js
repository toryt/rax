const nfa = require('./nfa')

function nfaStates2key (nfaStates) {
  return nfaStates.map(function (nfaState) { return nfaState.id }).sort().join('-')
}

function State (id, nfaStates, start) {
  this.id = id
  this.nfaStates = nfaStates
  this.key = nfaStates2key(nfaStates)
  this.transitions = {}
  this.start = start
  this.accepting = nfaStates.some(function (nfaState) { return nfaState instanceof nfa.Accepting })
}
Object.assign(
  State.prototype,
  {
    constructor: State,
    invariants: [
      function () { return typeof this.id === 'number' },
      function () { return Array.isArray(this.nfaStates) },
      function () { return this.nfaStates.every(function (nfaState) { return nfaState instanceof nfa.State }) },
      function () { return typeof this.key === 'string' },
      function () { return /((\d+K?)-)*(\d+K?)/g.test(this.key) },
      function () { return !!this.transitions },
      function () { return typeof this.transitions === 'object' },
      function () { return Object.keys(this.transitions).every(function (t) { return nfa.transitionToken.test(t) }) },
      function () { return typeof this.start === 'boolean' },
      function () { return typeof this.accepting === 'boolean' }
    ],
    addTransition: function (transition, state) {
      console.assert(nfa.transitionToken.test(transition))
      this.transitions[transition] = state
    },
    visit: function (visitor, acc, visited) {
      var self = this
      var lazyVisited = visited || []
      if (lazyVisited.indexOf(this) >= 0) {
        return acc
      }
      lazyVisited.push(this)
      var visitedAcc = visitor(acc, self)
      return Object.keys(self.transitions).reduce(
        function (acc, t) {
          return self.transitions[t].visit(visitor, acc, lazyVisited)
        },
        visitedAcc
      )
    },
    transitedState: function (char) {
      if (this.transitions[char]) {
        return this.transitions[char]
      }
      if ((this.transitions['?']) && nfa.starTransitionChar.test(char)) {
        return this.transitions['?']
      }
      if (this.transitions['**'] && nfa.transitionChar.test(char)) {
        return this.transitions['**']
      }
      return null
    },
    test: function (str) {
      console.log(this.id + ' (' + this.key + '): matching "' + str + '"')
      if (str.length === 0) {
        return this.accepting
      }
      console.log('  evaluating char "' + str.charAt(0) + '"')
      var nextState = this.transitedState(str.charAt(0))
      if (!nextState) {
        console.log('  no match')
        return false
      }
      console.log('  match: ' + nextState.id + ' (' + nextState.key + ')')
      return nextState.test(str.slice(1))
    }
  }
)

module.exports = {
  nfaStates2key: nfaStates2key,
  State: State
}
