const dfa = require('./dfa')

function transitions (charOrAny, nfaState) {
  if (charOrAny === '?') {
    return nfaState.token === '?' || nfaState.token === '**'
  }
  if (charOrAny === '**') {
    return nfaState.token === '**'
  }
  return nfaState.doesTransition(charOrAny)
}

function nfa2dfa (nfa) {
  var nextId = 0
  const dfaStart = new dfa.State(nextId, nfa.εClosure([]), true)
  nextId++
  const dfaStates = [dfaStart]
  const todo = [dfaStart]
  while (todo.length > 0) {
    var dfaState = todo.pop()
    console.log('working on ' + dfaState.id + ' (' + dfaState.key + ')')
    dfaState
      .nfaStates
      .forEach(function (nfaState) {
        const transitionsNfaToken = transitions.bind(null, nfaState.token)
        console.log('  handling ' + nfaState.id + ' ' + nfaState.token)
        if (nfaState.transition && !dfaState.transitions[nfaState.token]) {
          console.log('    "' + nfaState.token + '" has an NFA transition, and is not a DFA transition yet')
          const reachable = dfaState
            .nfaStates
            .filter(transitionsNfaToken)
            .reduce(function (acc, state) { return state.transition.εClosure(acc) }, [])
          console.log('    reachable: [' + reachable.map(function (nfaState) { return nfaState.id + ' ' + nfaState.token }).join(', ') + ']')
          const existingDfaStates = dfaStates.filter(function (dfaState) {
            return dfaState.key === dfa.nfaStates2key(reachable)
          })
          if (existingDfaStates.length > 0) {
            console.log('    state exists already (' + existingDfaStates.length + '): ' + existingDfaStates[0].key + ', adding transition for "' + nfaState.token + '"')
            dfaState.addTransition(nfaState.token, existingDfaStates[0])
          } else {
            const newState = new dfa.State(nextId, reachable)
            console.log('    new state: ' + newState.id + ' (' + newState.key + '), adding transition for "' + nfaState.token + '"')
            dfaState.addTransition(nfaState.token, newState)
            dfaStates.push(dfaState.transitions[nfaState.token])
            todo.push(dfaState.transitions[nfaState.token])
            nextId++
          }
        } else {
          console.log('    "' + nfaState.token + '" has no NFA transition or is already a DFA transition')
        }
      })
  }
  return dfaStart
}

module.exports = nfa2dfa
