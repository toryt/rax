const nfa = require('./nfa')

function handlePreviousState (previousState, currentState, openBraces) {
  if (previousState) {
    if (previousState.token === '{' || previousState.token === ',') {
      openBraces[0].open.addε(currentState)
    } else if (previousState.token === '}' || previousState.token === nfa.KLEENE_STAR) {
      previousState.addε(currentState)
    } else {
      previousState.transition = currentState
    }
  }
}

function rax2nfa (rax) {
  const splitter = /:|\/|\?|\*\*|\*|{|,|}|[a-zA-Z]|\d/g // must be created fresh
  var token = splitter.exec(rax)
  var start
  var openBraces = []
  var previousState
  var currentState = null
  while (token) {
    previousState = currentState
    var nfaToken = token[0] === '*' ? '?' : token[0]
    if (token.index === 0) {
      currentState = new nfa.Start(token.index, nfaToken)
      start = currentState
    } else {
      currentState = new nfa.NotAccepting(token.index, nfaToken)
    }
    handlePreviousState(previousState, currentState, openBraces)
    if (token[0] === '{') {
      openBraces.unshift({open: currentState, commas: []})
    } else if (token[0] === ',') {
      openBraces[0].commas.push(currentState)
    } else if (token[0] === '}') {
      openBraces[0].commas.forEach(function (comma) {
        comma.addε(currentState)
      })
      openBraces.shift()
    } else if (token[0] === '*' || token[0] === '**') {
      /* NOTE: http://www.tldp.org/LDP/GNU-Linux-Tools-Summary/html/x11655.htm
               '*' is [0..n], not [1..n] */
      // TODO: '**' only has special significance if it is the only thing in a path part. That is, 'a/**/b'
      //       will match 'a/x/y/b', but 'a/**b' will not
      //       (https://github.com/isaacs/minimatch)
      //       The token is '/**' at the end of the string, or '/**/' in the middle. /\/\*\*($|\/)/
      previousState = currentState
      currentState = new nfa.NotAccepting(token.index + 'K', nfa.KLEENE_STAR)
      previousState.transition = currentState
      previousState.addε(currentState)
      currentState.addε(previousState)
    }
    token = splitter.exec(rax)
  }
  previousState = currentState
  currentState = new nfa.Accepting(rax.length)
  console.assert(currentState instanceof nfa.Accepting)
  console.assert(previousState.token !== '{' && previousState.token !== ',')
  handlePreviousState(previousState, currentState, openBraces)
  return start
}

module.exports = rax2nfa
