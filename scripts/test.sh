#!/usr/bin/env bash

export NODE_ENV="ci"
echo 'node:' `node --version`
echo 'npm:' `npm --version`
npm install
echo 'branch:' `git rev-parse --abbrev-ref HEAD`
echo 'tags:' `git name-rev --tags --name-only $(git rev-parse HEAD)`
echo 'sha:' `git rev-parse --verify HEAD`
npm test
