/* eslint-env mocha */

const μm = require('micromatch')
const mm = require('minimatch')

/* eslint-env mocha */

function test (name, m) {
  describe(name, function () {
    if (m === μm.isMatch) {
      it('does match * with the empty string', function () {
        const glob = '/dir/*/something'
        const str = '/dir//something'
        // noinspection SpellCheckingInspection
        m(str, glob, {noext: false, nonegate: true}).must.be.true()
      })
    }
    if (m === mm) {
      it('does not match * with the empty string', function () {
        const glob = '/dir/*/something'
        const str = '/dir//something'
        // noinspection SpellCheckingInspection
        m(str, glob, {noext: false, nonegate: true}).must.be.false()
      })
    }
    it('does match ** with the empty string', function () {
      const glob = '/dir/**/something'
      const str = '/dir//something'
      // noinspection SpellCheckingInspection
      m(str, glob, {noext: false, nonegate: true}).must.be.true()
    })
    it('does not do partial match', function () {
      const glob = '/dir/something'
      const str = '/dir/something/else'
      // noinspection SpellCheckingInspection
      m(str, glob, {noext: false, nonegate: true}).must.be.false()
    })
    it('does not match ? with /', function () {
      const glob = '/dir/something?'
      const str = '/dir/something/'
      // noinspection SpellCheckingInspection
      m(str, glob, {noext: false, nonegate: true}).must.be.false()
    })
    it('does not match ? with ""', function () {
      const glob = '/dir/something?'
      const str = '/dir/something'
      // noinspection SpellCheckingInspection
      m(str, glob, {noext: false, nonegate: true}).must.be.false()
    })
    it('does match ? with non-slash char', function () {
      const glob = '/dir/something?'
      const str = '/dir/something1'
      // noinspection SpellCheckingInspection
      m(str, glob, {noext: false, nonegate: true}).must.be.true()
    })
    it('does not match ?* with the empty string', function () {
      const glob = '/dir/?*/something'
      const str = '/dir//something'
      // noinspection SpellCheckingInspection
      m(str, glob, {noext: false, nonegate: true}).must.be.false()
    })
    it('does match ** with the empty string', function () {
      const glob = '/dir/**?/something'
      const str = '/dir//something'
      // noinspection SpellCheckingInspection
      m(str, glob, {noext: false, nonegate: true}).must.be.false()
    })
    if (m === μm.isMatch) {
      it('only expands braces with noext: true', function () {
        const glob = '/{via,through}theroad'
        // noinspection SpellCheckingInspection
        const str = '/viatheroad'
        // noinspection SpellCheckingInspection
        m(str, glob, {noext: false, nonegate: true}).must.be.true()
        // noinspection SpellCheckingInspection
        m(str, glob, {noext: true, nonegate: true}).must.be.false()
      })
    }
    it('match has problems with ** in braces', function () {
      const glob1 = '/a/**'
      const glob2 = '/a{,/**}'
      const strA = '/a/whatever'
      const strB = '/a/whatever/comes/next'
      m(strA, glob1).must.be.true()
      m(strA, glob2).must.be.true()
      m(strB, glob1).must.be.true()
      if (m === μm.isMatch) {
        m(strB, glob2).must.be.false() // MUDO FAILS for micromatch https://github.com/micromatch/micromatch/issues/113
      }
      if (m === mm) {
        m(strB, glob2).must.be.true()
      }
    })

    const patterns1 = [
      'GET:/shop/5630A4//cliANY/duct/sale/whatever/comes/next',
      '{GET,PUT}:/shop/5630A4//cliANY/duct/sale/whatever/comes/next',
      '{GET,P{OS,U}T}:/shop/5630A4//cliANY/duct/sale/whatever/comes/next',
      '{GET,P{OS,U}T,DELETE}:/shop/5630A4//cliANY/duct/sale/whatever/comes/next',
      '{{GE,P{OS,U}}T,DELETE}:/shop/5630A4//cliANY/duct/sale/whatever/comes/next',
      '{{GE,P{OS,U}}T,DELETE}:/shop/{2345,5630A4}//cliANY/duct/sale/whatever/comes/next',
      '{{GE,P{OS,U}}T,DELETE}:/shop/{2345,5630?4}//cliANY/duct/sale/whatever/comes/next',
      '{{GE,P{OS,U}}T,DELETE}:/shop/{2345,56{7,{1,2,3,4,5},9}0?4}//cliANY/duct/sale/whatever/comes/next'
    ]
    const strA = 'GET:/shop/5630A4//cliANY/duct/sale/whatever/comes/next'
    patterns1.forEach(function (p) {
      it('matches ' + p, function () {
        m(strA, p).must.be.true()
      })
    })

    const patterns2 = [
      '{{GE,P{OS,U}}T,DELETE}:/shop/{2345,56{7,{1,2,3,4,5},9}0?4}/*/cliANY/duct/sale/whatever/comes/next',
      '{{GE,P{OS,U}}T,DELETE}:/shop/{2345,56{7,{1,2,3,4,5},9}0?4}/*/cli*/duct/sale/whatever/comes/next',
      '{{GE,P{OS,U}}T,DELETE}:/shop/{2345,56{7,{1,2,3,4,5},9}0?4}/*/cli*/*duct/sale/whatever/comes/next',
      '{{GE,P{OS,U}}T,DELETE}:/shop/{2345,56{7,{1,2,3,4,5},9}0?4}/*/cli*/*duct/sale/**'
    ]
    const strB = 'GET:/shop/5630A4/a/cliANY/duct/sale/whatever/comes/next'
    patterns2.forEach(function (p) {
      it('matches ' + p, function () {
        m(strB, p).must.be.true()
      })
    })

    const pattern3 = '{{GE,P{OS,U}}T,DELETE}:/shop/{2345,56{7,{1,2,3,4,5},9}0?4}/*/cli*/*duct/sale{,/**}'
    if (m === μm.isMatch) {
      it('μm has problems with ** in {}: ' + pattern3, function () {
        μm.isMatch(strB, pattern3).must.be.false()
      })
    }
    if (m === mm) {
      it('matches ' + pattern3, function () {
        mm(strB, pattern3).must.be.true()
      })
    }
  })
}

test('minimatch', mm)
test('micromatch', μm.isMatch)
