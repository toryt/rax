/* eslint-env mocha */

const rax2nfa = require('../../lib/rax2nfa')
const dfa2dot = require('../../lib/dfa2dot')
const writeFile = require('write')
const path = require('path')
const nfa2dfa = require('../../lib/nfa2dfa')
const mm = require('micromatch')

const complexRax = '{{GE,P{OS,U}}T,DELETE}:/shop/{2345,56{7,{1,2,3,4,5},9}0?4}/*/cli*/*duct/sale{,/**}'
const starRax1 = 'GET:/*bra'
const starRax2 = 'GET:/{*duct,}bra'

const location = path.parse(module.filename)
const outputDir = path.join(location.dir, 'output', location.name)

describe('nfa2dfa', function () {
  it('transfers a complex rax into a dfa', function () {
    const nfa = rax2nfa(complexRax)
    const dfa = nfa2dfa(nfa)
    const dot = dfa2dot(dfa)
    return writeFile(path.join(outputDir, 'complexRax.dot'), dot)
  })
  it('returns a match for a matching pattern', function () {
    const nfa = rax2nfa(complexRax)
    const dfa = nfa2dfa(nfa)
    const str = 'GET:/shop/5630A4//cliANY/duct/sale/whatever/comes/next'
    // m(str, complexRax).must.be.true() // TODO micromatch has an issue here with {,/**}; minimatch has an issue here with /*/ ~ //; we do better than both
    const result = dfa.test(str)
    result.must.be.true()
  })
  it('returns a failure for a non-matching pattern', function () {
    const nfa = rax2nfa(complexRax)
    const dfa = nfa2dfa(nfa)
    const str = 'GET:/shop/5630A4//cliANY/duct/sile/whatever/comes/next'
    // noinspection SpellCheckingInspection
    mm.isMatch(str, complexRax, {nonegate: true}).must.be.false()
    const result = dfa.test(str)
    result.must.be.false()
  })
  it('matches a star rax', function () {
    const dfa = nfa2dfa(rax2nfa(starRax1))
    const dot = dfa2dot(dfa)
    return writeFile(path.join(outputDir, 'starRax1.dot'), dot)
      .then(function () {
        // noinspection SpellCheckingInspection
        const str = 'GET:/acdabcduabcducabcductductbra'
        // noinspection SpellCheckingInspection
        mm.isMatch(str, starRax1, {nonegate: true}).must.be.true()
        const result = dfa.test(str)
        result.must.be.true()
      })
  })
  it('matches a star rax too', function () {
    const dfa = nfa2dfa(rax2nfa(starRax1))
    // noinspection SpellCheckingInspection
    const str = 'GET:/bra'
    // noinspection SpellCheckingInspection
    mm.isMatch(str, starRax1, {nonegate: true}).must.be.true()
    const result = dfa.test(str)
    result.must.be.true()
  })
  it('matches a star rax 2', function () {
    const dfa = nfa2dfa(rax2nfa(starRax2))
    const dot = dfa2dot(dfa)
    return writeFile(path.join(outputDir, 'starRax2.dot'), dot)
      .then(function () {
        // noinspection SpellCheckingInspection
        const str = 'GET:/acdabcduabcducabcductductbra'
        // noinspection SpellCheckingInspection
        mm.isMatch(str, starRax2, {nonegate: true}).must.be.true()
        const result = dfa.test(str)
        result.must.be.true()
      })
  })
  it('matches a star rax 2 too', function () {
    const dfa = nfa2dfa(rax2nfa(starRax2))
    // noinspection SpellCheckingInspection
    const str = 'GET:/bra'
    // noinspection SpellCheckingInspection
    mm.isMatch(str, starRax2, {nonegate: true}).must.be.true()
    const result = dfa.test(str)
    result.must.be.true()
  })
})
