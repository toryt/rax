/* eslint-env mocha */

const rax2nfa = require('../../lib/rax2nfa')
const nfa2dot = require('../../lib/nfa2dot')
const writeFile = require('write')
const path = require('path')
const mm = require('micromatch')

const complexRax = '{{GE,P{OS,U}}T,DELETE}:/shop/{2345,56{7,{1,2,3,4,5},9}0?4}/*/cli*/*duct/sale/**'
const starRax1 = 'GET:/*bra'
const starRax2 = 'GET:/{*duct,}bra'

const location = path.parse(module.filename)
const outputDir = path.join(location.dir, 'output', location.name)

describe('rax2nfa', function () {
  it('parses a complex rax', function () {
    const nfa = rax2nfa(complexRax)
    const dot = nfa2dot(nfa)
    return writeFile(path.join(outputDir, 'complexRax.dot'), dot)
  })
  it('returns a match for a matching pattern', function () {
    const nfa = rax2nfa(complexRax)
    const str = 'GET:/shop/5630A4//cliANY/duct/sale/whatever/comes/next'
    // noinspection SpellCheckingInspection
    mm.isMatch(str, complexRax, {nonegate: true}).must.be.true()
    const result = nfa.test(str)
    result.must.be.true()
  })
  it('returns a failure for a non-matching pattern', function () {
    const nfa = rax2nfa(complexRax)
    const str = 'GET:/shop/5630A4//cliANY/duct/sile/whatever/comes/next'
    // noinspection SpellCheckingInspection
    mm.isMatch(str, complexRax, {nonegate: true}).must.be.false()
    const result = nfa.test(str)
    result.must.be.false()
  })
  it('matches a star rax', function () {
    const nfa = rax2nfa(starRax1)
    const dot = nfa2dot(nfa)
    return writeFile(path.join(outputDir, 'starRax1.dot'), dot)
      .then(function () {
        // noinspection SpellCheckingInspection
        const str = 'GET:/acdabcduabcducabcductductbra'
        // noinspection SpellCheckingInspection
        mm.isMatch(str, starRax1, {noext: false, nonegate: true}).must.be.true()
        const result = nfa.test(str)
        result.must.be.true()
      })
  })
  it('matches a star rax too', function () {
    const nfa = rax2nfa(starRax1)
    // noinspection SpellCheckingInspection
    const str = 'GET:/bra'
    // noinspection SpellCheckingInspection
    mm.isMatch(str, starRax1, {noext: false, nonegate: true}).must.be.true()
    const result = nfa.test(str)
    result.must.be.true()
  })
  it('matches a star rax 2', function () {
    const nfa = rax2nfa(starRax2)
    const dot = nfa2dot(nfa)
    return writeFile(path.join(outputDir, 'starRax2.dot'), dot)
      .then(function () {
        // noinspection SpellCheckingInspection
        const str = 'GET:/acdabcduabcducabcductductbra'
        // noinspection SpellCheckingInspection
        mm.isMatch(str, starRax2, {noext: false, nonegate: true}).must.be.true()
        const result = nfa.test(str)
        result.must.be.true()
      })
  })
  it('matches a star rax 2 too', function () {
    const nfa = rax2nfa(starRax2)
    // noinspection SpellCheckingInspection
    const str = 'GET:/bra'
    // noinspection SpellCheckingInspection
    mm.isMatch(str, starRax2, {noext: false, nonegate: true}).must.be.true()
    const result = nfa.test(str)
    result.must.be.true()
  })
})
